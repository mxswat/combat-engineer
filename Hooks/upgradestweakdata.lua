local data = UpgradesTweakData.init
function UpgradesTweakData:init(tweak_data)
    data(self, tweak_data)

    -- explosion_on_death
    self.values.sentry_gun.explosion_on_death = {45, 75}

    self.definitions.explosion_on_death_1 = {
        category = "feature",
        name_id = "menu_sentry_gun_explosion_on_death_1",
        upgrade = {
            category = "sentry_gun",
            upgrade = "explosion_on_death",
            value = 1
        }
    }
    -- explosion_on_death end

    -- sentry_on_kill_hp_regen
    self.values.sentry_gun.sentry_on_kill_hp_regen_1 = {true} -- 5%
    self.values.sentry_gun.sentry_on_kill_hp_regen_2 = {true} -- 10%

    self.definitions.sentry_on_kill_hp_regen_1 = {
        category = "feature",
        name_id = "menu_sentry_gun_sentry_on_kill_hp_regen",
        upgrade = {
            category = "sentry_gun",
            upgrade = "sentry_on_kill_hp_regen_1",
            value = 0
        }
    }

    self.definitions.sentry_on_kill_hp_regen_2 = {
        category = "feature",
        name_id = "menu_sentry_gun_sentry_on_kill_hp_regen",
        upgrade = {
            category = "sentry_gun",
            upgrade = "sentry_on_kill_hp_regen_2",
            value = 1
        }
    }
    -- sentry_on_kill_hp_regen end
end
