Hooks:PostHook(SentryGunBase, "on_death", "on_death_modded", function(self)
    managers.chat:_receive_message(1, 'MX', 'A turret got destroyed', Color(tostring('00ff00')))
    if managers.player:has_category_upgrade("sentry_gun", "explosion_on_death") then
        local bodies = World:find_units_quick("sphere", self._unit:position(), '750', managers.slot:get_mask("enemies"))
        local col_ray = { }
        col_ray.ray = Vector3(1, 0, 0)
        col_ray.position = self._unit:position()
        local action_data = {
            variant = "explosion",
            damage = 10,
            attacker_unit = managers.player:player_unit(),
            col_ray = col_ray
        }
        for _, hit_unit in ipairs(bodies) do
            if hit_unit:character_damage() then
                hit_unit:character_damage():damage_explosion(action_data)
            end
        end
    end
end)

Hooks:PostHook(SentryGunBase, "update", "update_modded", function(self, unit, t, dt)
    
end)