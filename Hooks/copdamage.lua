local chk_killshot_original = CopDamage.chk_killshot
function CopDamage:chk_killshot(attacker_unit, ...)
  if alive(attacker_unit) then
    local key = tostring(attacker_unit:key())
    if attacker_unit:in_slot(25) and managers.gameinfo and managers.gameinfo:get_sentries(key) and managers.player:player_unit() and attacker_unit then
      if managers.player:has_category_upgrade("sentry_gun", "sentry_on_kill_hp_regen_2") then
        managers.player:player_unit():character_damage():restore_health(0.075)
      elseif managers.player:has_category_upgrade("sentry_gun", "sentry_on_kill_hp_regen_1") then
        managers.player:player_unit():character_damage():restore_health(0.15)
      end
    end
  end
  return chk_killshot_original(self, attacker_unit, ...)
end
