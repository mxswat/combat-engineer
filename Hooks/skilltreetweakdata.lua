--This is a OLD post HOOK
local data = SkillTreeTweakData.init
function SkillTreeTweakData:init(tweak_data)
    data(self, tweak_data)


    local new_default_upgrades = {
        "sentry_gun_silent", -- engineering Base
        "sentry_gun_armor_multiplier" -- Eco sentry Aced
    }
    for idx in pairs(new_default_upgrades) do
        table.insert(self.default_upgrades, new_default_upgrades[idx])
    end

    self.skills.tower_defense = {
        {
            upgrades = {"sentry_on_kill_hp_regen_1"},
            cost = self.costs.default

        },
        {
            upgrades = {"sentry_on_kill_hp_regen_2"},
            cost = self.costs.pro
        },
        name_id = "menu_tower_defense_mx",
        desc_id = "menu_tower_defense_mx_desc",
		icon_xy = { -- earth icon
			9,
			2
		}
    }

	self.skills.defense_up = {
		{
			upgrades = {"sentry_gun_shield"},
			cost = self.costs.hightier
		},
		{
			upgrades = {"explosion_on_death_1"},
			cost = self.costs.hightierpro
		},
		name_id = "menu_defense_up_mx",
		desc_id = "menu_defense_up_mx_desc",
		icon_xy = {
			9,
			0
		}
    }
    
    self.skills.eco_sentry = {
		{
			upgrades = {"sentry_gun_cost_reduction_1"},
			cost = self.costs.hightier
		},
		{
			upgrades = {"sentry_gun_cost_reduction_2"},
			cost = self.costs.hightierpro
		},
		name_id = "menu_eco_sentry_mx",
		desc_id = "menu_eco_sentry_mx_desc",
		icon_xy = {
			9,
			3
		}
    }
    
    self.skills.engineering = {
		{
			upgrades = {"sentry_gun_silent", "sentry_gun_quantity_1"},
			cost = self.costs.hightier
		},
		{
			upgrades = {
				"sentry_gun_ap_bullets",
				"sentry_gun_fire_rate_reduction_1", "sentry_gun_quantity_2"
			},
			cost = self.costs.hightierpro
		},
		name_id = "menu_engineering_mx",
        desc_id = "menu_engineering_mx_desc",
        icon_xy = {
			9,
			5
		}
	}

end
