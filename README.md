# Technician V2 #

New tier 4 skill `Overload` will arrive to replace `dispencer module`!

In full auto:
Sentry regenerates ammo on kill!
After each kill the turret will go into `Overload` for 5 seconds and it's rate of fire will be increased by 10% for each stored kill.
`Overload` Has a cooldown of 15 seconds.

In AP mode:
Each shot now have a 50% chance to put enemies of fire and 50% of exploding.